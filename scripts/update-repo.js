const glob = require('glob');
const fs = require('fs');
const path = require('path');

const gitUrl = 'https://bitbucket.org/entrptaher';
const author = `Md. Abu Taher <entrptaher@gmail.com> (${gitUrl})`;
const basePath = '/utility-scripts/src/master';
const baseRepo = gitUrl + basePath;

glob('packages/*/package.json', (err, files) => {
  for (const filePath of files) {
    const [parent, pkg] = filePath.split('/');
    const newData = {
      author,
      license: 'MIT',
      repository: `${baseRepo}/${parent}/${pkg}`,
    };
    const data = Object.assign(
      {},
      JSON.parse(fs.readFileSync(path.resolve(filePath), 'utf-8')),
      newData,
    );
    fs.writeFileSync(path.resolve(filePath), JSON.stringify(data, true, 2));
  }
});
