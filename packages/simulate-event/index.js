module.exports = function simulateEvent(eventx, elem) {
  const event = new MouseEvent(eventx, {
    view: window,
    bubbles: true,
    cancelable: true,
  });
  const cb = elem;
  const cancelled = !cb.dispatchEvent(event);
  if (cancelled) {
    // A handler called preventDefault.
    // we probably need to log this to user
    window.console.log(eventx, 'event cancelled by client');
  } else {
    // None of the handlers called preventDefault.
    // we probably need to log this to user
    window.console.log(eventx, 'event not cancelled by client');
  }
};
