A wrapper around puppeteer to chain and queue the tasks

# USAGE

```
// app.js
const puppeteer = require('puppeteer');
const Chain = require("pup-chain");

(async() => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const chain = await new Chain(page);

  // series data
  await chain
    .goto("https://example.com")
    .url()
    .title()

  let SeriesData = await chain.series();
  [response, url, title] = SeriesData;
  console.dir({url, title});
  // { url: 'https://example.com/', title: 'Example Domain' }

  // chain run
  await chain
    .goto("https://example.org")
    .url()

  let chainData = await chain.run()
  console.dir({url: chainData})
  // { url: 'https://example.org/' }

  browser.close();
})();
```
