const ObjProperties = require('@entrptaher/simple-property-retriever');
const pWaterfall = require('p-waterfall');
const pSeries = require('p-series');

class Chain {
  constructor(initiator) {
    this.chain = [];
    const addMethods = (ref) => {
      this.ref = ref;
      const properties = ObjProperties.getOwnNonenumerables(ref.__proto__);
      for (const property of properties) {
        this[property] = function (...args) {
          this.chain.push(() => this.ref[property](...args));
          return this;
        };
      }
      return this.ref;
    };

    addMethods(initiator);
  }

  run() {
    const chain = [...this.chain];
    this.chain = [];
    return pWaterfall(chain);
  }

  series() {
    // TODO: Remove Duplicate codes
    const chain = [...this.chain];
    this.chain = [];
    return pSeries(chain);
  }
}

module.exports = Chain;
