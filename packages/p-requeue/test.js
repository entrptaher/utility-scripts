const PQueue = require('p-queue');

const queue = new PQueue({ concurrency: 1 });

function goodFn() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve('Should Resolve');
    }, 1000);
  });
}

function errorFn() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new Error('Should Throw'));
    }, 100);
  });
}

const addToQueue = require('./index.js');

for (let i = 0; i < 5; i++) {
  addToQueue(queue, goodFn).then(console.log);
}
