module.exports = function returnReplaced(returntemplate, targetAttr, filler) {
  returntemplate = JSON.parse(JSON.stringify(returntemplate));
  returntemplate[targetAttr] = filler;
  return returntemplate;
};
