module.exports = function repeatArray(arr, count) {
  const ln = arr.length;
  const b = [];
  for (let i = 0; i < count; i++) {
    b.push(arr[i % ln]);
  }
  return b;
};
