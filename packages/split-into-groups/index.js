module.exports = function splitIntoGroups(arr, size) {
  return arr.map((e, i) => (i % size === 0 ? arr.slice(i, i + size) : null)).filter(e => e);
};
