Get Object properties, taken from MDN.

### Example Usage

```js
class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }

  get area() {
    return this.calcArea();
  }

  calcArea() {
    return this.height * this.width;
  }
}

console.log(ret.getOwnNonenumerables(Rectangle.prototype));
// [ 'constructor', 'area', 'calcArea' ]
```
