const flattenObject = require('@entrptaher/flatten-object');

module.exports = function oneStepFlatten(obj) {
  return new Promise((resolve, reject) => {
    try {
      const data = obj.map(elem => flattenObject(elem));
      Promise.all(data).then((data) => {
        resolve(data);
      });
    } catch (e) {
      reject({});
    }
  });
};
