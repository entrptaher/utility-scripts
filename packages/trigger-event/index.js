/**
 * Trigger the specified event on the specified element.
 * @param  {Object} elem  the target element.
 * @param  {String} event the type of the event (e.g. 'click').
 */

module.exports = function triggerEvent(elem, event) {
  const clickEvent = new Event(event); // Create the event.
  elem.dispatchEvent(clickEvent); // Dispatch the event.
};
