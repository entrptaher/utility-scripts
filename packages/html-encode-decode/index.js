const replaceMap = [
  ['&', '&amp;'],
  ['<', '&lt;'],
  ['>', '&gt;'],
  ['"', '&quot;'],
  ["'", '&#39;'],
  ['/', '&#x2F;'],
  ['`', '&#x60;'],
  ['=', '&#x3D;'],
];

function replacer(input, encode) {
  let replace = 1;


  let replaceWith = 0;
  if (encode) {
    (replace = 0), (replaceWith = 1);
  }
  for (const replacerObj of replaceMap) {
    input = String(input).replace(
      new RegExp(replacerObj[replace], 'gim'),
      replacerObj[replaceWith],
    );
  }
  return input;
}

module.exports = {
  encode(input) {
    return replacer(input, true);
  },
  decode(input) {
    return replacer(input);
  },
};
