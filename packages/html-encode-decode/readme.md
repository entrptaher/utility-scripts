```js
const encoder = require('./index');

const encoded = encoder.encode('<p>Hello World</p>');
const decoded = encoder.decode(encoded);
console.log({ encoded, decoded });
```