function repeatFillArray(arr, arr2) {
  if (arr.length < arr2.length) {
    arr = arr.concat(arr);
    return repeatFillArray(arr, arr2);
  }
  return arr.slice(0, arr2.length);
}
const arr = [1, 2, 3, 4];
const arr2 = [1, 2, 3, 4, 5];

console.log(repeatFillArray(arr, arr2));
