const Data = require('./data.json');

function getRandomArray(array) {
  return array[Math.floor(Math.random() * array.length)];
}

const randomDevice = {
  getRandomMobile() {
    return getRandomArray(Data.mobiles);
  },
  getRandomTablet() {
    return getRandomArray(Data.tablets);
  },
  getRandomDesktop() {
    const resolution = getRandomArray(Data.desktops.resolutions).split('x');
    return {
      deviceName: 'Random Desktop',
      width: resolution[0],
      height: resolution[1],
      userAgent: getRandomArray(Data.desktops.userAgents),
      deviceScaleFactor: 0,
      touch: 'false',
      mobile: 'false',
    };
  },
  getRandomAlexa() {
    const resolution = getRandomArray(Data.desktops.resolutions).split('x');
    return {
      deviceName: 'Random Alexa',
      width: resolution[0],
      height: resolution[1],
      userAgent: getRandomArray(Data.alexa.userAgents),
      deviceScaleFactor: 0,
      touch: 'false',
      mobile: 'false',
    };
  },
  getRandomMixed() {
    return randomDevice[getRandomArray(Object.keys(randomDevice))]();
  },
};

module.exports = randomDevice;
