const simulateEvent = require('@entrptaher/simulate-event');

module.exports = function simulateType(selector, value) {
  const elem = document.querySelector(selector);
  elem.value = value;
  simulateEvent('click', elem);
  simulateEvent('focus', elem);
  simulateEvent('keydown', elem);
  simulateEvent('keypress', elem);
  simulateEvent('mousedown', elem);
  simulateEvent('compositionend', elem);
  simulateEvent('compositionstart', elem);
  simulateEvent('blur', elem);
};
