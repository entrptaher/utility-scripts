module.exports = function setSelectByText(selector, text) {
  // Loop through sequentially//
  const ele = document.querySelector(selector);
  for (let ii = 0; ii < ele.length; ii++) {
    if (ele.options[ii].text === text) {
      // Found!
      ele.options[ii].selected = true;
      return true;
    }
  }
  return false;
};
