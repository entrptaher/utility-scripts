const absolute = require('@entrptaher/absolute-url');
const GetSiteRoot = require('@entrptaher/get-site-root');

module.exports = function checksetRelative(url) {
  const r = new RegExp('^(?:[a-z]+:)?//', 'i');
  // if the url is not absolute
  if (!r.test(url)) {
    return absolute(GetSiteRoot(), url);
  }
  return url;
};
