// Select by value, http://stackoverflow.com/a/16444979
function setSelectByValue(selector, val) {
  // Loop through sequentially//
  if (!val) {
    return false;
  }
  const ele = document.querySelector(selector);
  // console.log(selector, val, ele, ele.options.length);

  if (ele) {
    for (let ii = 0; ii < ele.length; ii++) {
      if (ele.options[ii].value === val) {
        // Found!
        ele.options[ii].selected = true;

        setTimeout(() => {
          // trigger the change of select options
          reactTriggerChange(ele);
        }, 50);

        return true;
      }
    }
  }
  return false;
}

module.exports = setSelectByValue;
