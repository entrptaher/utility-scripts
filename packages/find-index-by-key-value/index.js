module.exports = function findIndexByKeyValue(arraytosearch, key, valuetosearch) {
  for (let i = 0; i < arraytosearch.length; i++) {
    if (arraytosearch[i][key] === valuetosearch) {
      return i;
    }
  }
  return null;
};
