module.exports = function findIfStartsWith(string, needle) {
  return string.indexOf(needle) === 0;
};
