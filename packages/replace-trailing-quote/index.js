module.exports = function replaceTrailingQuote(string) {
  return string.replace(/(^&quot;)|(&quot;)$/gm, '');
};
